﻿using UnityEngine;
using UnityEngine.UI;       // add this if you want to interact with the new UI elements like Buttons or Text
using System.Collections;
using UnityEngine.SceneManagement;

public class sceneloader : MonoBehaviour {

	public string LevelToLoad;

	void OnTriggerEnter(){
		AwplaysClicked ();
	}

	public void AwplaysClicked()
	{
		SceneManager.LoadScene (LevelToLoad);
	}
}